import {expect, describe, test} from '@jest/globals'
import { getStatusAndClass, turnWorkshopArrayIntoObject, lastUpdatedOn, splitFileFormats } from "./productions.js"

import { STATUS_EXPORTED, STATUS_PLATON_PENDING, STATUS_PLATON_AWAITING, STATUS_TO_CONVERT, STATUS_PROCESSING, STATUS_UNKNOWN, STATUS_STANDBY, STATUS_DONE, STATUSES } from "./productions.js"

describe('getFileStatus', () => {
    test('return the correct status depending on the given parameters', () => {
        expect(getStatusAndClass({ metas: { outputs: [] }})).toHaveProperty('_status_id', STATUS_UNKNOWN)

        // By default, a record is `STATUS_PLATON_PENDING` (its XML file has yet to be asked)
        expect(getStatusAndClass({ prc: 'data', metas: { outputs: [] } })).toHaveProperty('_status_id', STATUS_PLATON_PENDING)
        expect(getStatusAndClass({ prc: 'data', metas: { outputs: ["un fichier"] } })).toHaveProperty('_status_id', STATUS_PLATON_PENDING)

        // From now on, we can put a production in `STATUS_STANDBY`
        // It can also be the case when an awaited XML file is "not working"
        // By default, a record is `STATUS_PLATON_PENDING` (its XML file has yet to be asked)
        expect(getStatusAndClass({ prc: 'data', metas: { outputs: [], statut: STATUS_STANDBY } })).toHaveProperty('_status_id', STATUS_STANDBY)
        expect(getStatusAndClass({ prc: 'data', metas: { outputs: ["un fichier"], statut: STATUS_STANDBY } })).toHaveProperty('_status_id', STATUS_STANDBY)

        // Then, when an XML file is asked, we are in `STATUS_PLATON_AWAITING`
        expect(getStatusAndClass({ prc: 'data', metas: { outputs: [], statut: STATUS_PLATON_AWAITING } })).toHaveProperty('_status_id', STATUS_PLATON_AWAITING)

        // A "Previous Status" can be appended to a valid status, in order to help switch back to it
        expect(getStatusAndClass({ prc: 'data', metas: { outputs: [], statut: STATUS_PLATON_AWAITING } })).toMatchObject({
          previousStatus: undefined,
          _status_id: STATUS_PLATON_AWAITING,
        })
        expect(getStatusAndClass({ prc: 'data', metas: { outputs: [], statut: `${STATUS_STANDBY}|${STATUS_PLATON_AWAITING}` } })).toMatchObject({
          previousStatus: STATUS_PLATON_AWAITING,
          _status_id: STATUS_STANDBY,
        })
        expect(getStatusAndClass({ prc: 'xml', metas: { outputs: [], statut: `${STATUS_STANDBY}|${STATUS_PLATON_AWAITING}` } })).toMatchObject({
          previousStatus: STATUS_PLATON_AWAITING,
          _status_id: STATUS_STANDBY,
        })
        expect(getStatusAndClass({ prc: 'xml', metas: { outputs: [], statut: `${STATUS_STANDBY}|${STATUS_PROCESSING}` } })).toMatchObject({
          previousStatus: STATUS_PROCESSING,
          _status_id: STATUS_STANDBY,
        })

        // Then, when an XML file is asked, we are in `STATUS_PLATON_AWAITING`
        expect(getStatusAndClass({ prc: 'data', metas: { outputs: [], statut: STATUS_PLATON_AWAITING } })).toHaveProperty('_status_id', STATUS_PLATON_AWAITING)
        expect(getStatusAndClass({ prc: 'data', metas: { outputs: ["un fichier"], statut: STATUS_PLATON_AWAITING } })).toHaveProperty('_status_id', STATUS_PLATON_AWAITING)

        // Once its declared received and well formed, we are in `STATUS_TO_CONVERT`
        expect(getStatusAndClass({ prc: 'data', metas: { outputs: [], statut: STATUS_TO_CONVERT } })).toHaveProperty('_status_id', STATUS_TO_CONVERT)
        expect(getStatusAndClass({ prc: 'data', metas: { outputs: ["un fichier"], statut: STATUS_TO_CONVERT } })).toHaveProperty('_status_id', STATUS_TO_CONVERT)

        // When we remove a document, and update its meta, the XML document is kept
        // So we are "temporarily" (which means, "maybe forerever") accepting the fact it is not yet processing
        expect(getStatusAndClass({ prc: 'xml', metas: { outputs: [], statut: STATUS_PROCESSING }})).toHaveProperty('_status_id', STATUS_TO_CONVERT)
        expect(getStatusAndClass({ prc: 'xml', metas: { outputs: [], statut: STATUS_TO_CONVERT }})).toHaveProperty('_status_id', STATUS_TO_CONVERT)
        expect(getStatusAndClass({ prc: 'xml', metas: { outputs: ["un fichier"], statut: STATUS_PROCESSING }})).toHaveProperty('_status_id', STATUS_TO_CONVERT)
        expect(getStatusAndClass({ prc: 'xml', metas: { outputs: ["un fichier"], statut: STATUS_TO_CONVERT }})).toHaveProperty('_status_id', STATUS_TO_CONVERT)

        // When we are in `STATUS_TO_CONVERT`, and we have processed an XML file, thus we have a "workshop", we are in STATUS_PROCESSING
        expect(getStatusAndClass({ prc: 'xml', metas: { outputs: [], statut: STATUS_PROCESSING, workshop: { srcId: 'id:0f1234' } }})).toHaveProperty('_status_id', STATUS_PROCESSING)
        expect(getStatusAndClass({ prc: 'xml', metas: { outputs: [], statut: '', workshop: { srcId: 'id:0f1234' } }})).toHaveProperty('_status_id', STATUS_PROCESSING)
        expect(getStatusAndClass({ prc: 'xml', metas: { outputs: [], workshop: { srcId: 'id:0f1234' } }})).toHaveProperty('_status_id', STATUS_PROCESSING)

        // When we are in `STATUS_PROCESSING`, and we have processed an XML file and we have at least one output, we are in `STATUS_EXPORTED`
        expect(getStatusAndClass({ prc: 'xml', metas: { outputs: ["un fichier"], statut: STATUS_PROCESSING, workshop: { srcId: 'id:0f1234' } }})).toHaveProperty('_status_id', STATUS_EXPORTED)
        expect(getStatusAndClass({ prc: 'xml', metas: { outputs: ["un fichier"], statut: '', workshop: { srcId: 'id:0f1234' } }})).toHaveProperty('_status_id', STATUS_EXPORTED)
        expect(getStatusAndClass({ prc: 'xml', metas: { outputs: ["un fichier"], workshop: { srcId: 'id:0f1234' } }})).toHaveProperty('_status_id', STATUS_EXPORTED)

        // Only a human can decide to turn a `STATUS_EXPORTED` into `STATUS_DONE`
        expect(getStatusAndClass({ prc: 'xml', metas: { outputs: [], statut: STATUS_DONE, workshop: { srcId: 'id:0f1234' } }})).toHaveProperty('_status_id', STATUS_DONE)
        expect(getStatusAndClass({ prc: 'xml', metas: { outputs: ["un fichier"], statut: STATUS_DONE, workshop: { srcId: 'id:0f1234' } }})).toHaveProperty('_status_id', STATUS_DONE)
    })

    test('return the status id and its values', () => {
      expect(getStatusAndClass({ metas: { outputs: [] }})).toStrictEqual({
        _status_id: STATUS_UNKNOWN,
        _status: STATUSES[STATUS_UNKNOWN],
        previousStatus: undefined
      })
    })
})

describe('turnWorkshopArrayIntoObject', () => {
  test('turn an array into an object', () => {
    const row = [
      '/FR-AVH-ISBN-1234.book',
      'working',
      'Titans',
      'true',
      '2021'
    ]

    const columns = [
      'srcUri',
      'lcSt',
      'itTi',
      'hasResume',
      'publishingYear'
    ]

    expect(turnWorkshopArrayIntoObject(row, columns)).toEqual({
      srcUri: '/FR-AVH-ISBN-1234.book',
      lcSt: 'working',
      itTi: 'Titans',
      hasResume: 'true',
      publishingYear: '2021'
    })
  })
})


describe('lastUpdatedOn', () => {
  test('une production à convertir', () => {
    const metas = {
      deployDate: 1625560368927
    }

    expect(lastUpdatedOn(metas)).toEqual(1625560368927)
  })

  test('une production en cours', () => {
    const metas = {
      deployDate: 1625560368927,
      updateDate: 1629705755885
    }

    expect(lastUpdatedOn(metas)).toEqual(1625560368927)
  })

  test('une production exportée', () => {
    const metas = {
      deployDate: 1625560368927,
      updateDate: 1625560369000,
      outputs: [
        { metas: { deployDate: 1625673490462 }},
        { metas: { deployDate: 1625680173145 }},
        { metas: { deployDate: 1625680172082 }},
        { metas: { deployDate: 1625680170849 }},
        { metas: { deployDate: 1625680189839 }},
        { metas: { deployDate: 1625680077486 }}
      ]
    }

    expect(lastUpdatedOn(metas)).toEqual(1625680189839)
  })

  test('une production terminée', () => {
    const metas = {
      deployDate: 1625560368927,
      updateDate: 1629705755885,
      dateEnd: '1629714379901',
      outputs: [
        { metas: { deployDate: 1625673490462 }},
        { metas: { deployDate: 1625680173145 }},
      ]
    }

    expect(lastUpdatedOn(metas)).toEqual(1629714379901)
  })
})

describe('splitFileFormats()', () => {
  test('parses a metadata into a structured object', () => {
    expect(splitFileFormats('')).toEqual({ source: [], _source: {knownFormats: [], other: null }})
    expect(splitFileFormats('PDF')).toEqual({ source: ['PDF'], _source: { knownFormats: ['PDF'], other: null }})
    expect(splitFileFormats('PDF|XML')).toEqual({ source: ['PDF', 'XML'], _source: { knownFormats: ['PDF', 'XML'], other: null }})
  })

  test('parses an unknown value into an other kind', () => {
    expect(splitFileFormats('Test')).toEqual({ source: ['Test'], _source: { knownFormats: [], other: 'Test' }})
    expect(splitFileFormats('PDF|Test')).toEqual({ source: ['PDF', 'Test'], _source: { knownFormats: ['PDF'], other: 'Test' }})
    expect(splitFileFormats('PDF|Test|')).toEqual({ source: ['PDF', 'Test'], _source: { knownFormats: ['PDF'], other: 'Test' }})
    expect(splitFileFormats('PDF|Test|Other')).toEqual({ source: ['PDF', 'Test', 'Other'], _source: { knownFormats: ['PDF'], other: 'Other' }})
  })
})
