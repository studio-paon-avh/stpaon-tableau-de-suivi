import {expect, describe, test} from '@jest/globals'
import { cleanVolume, cleanPages, cleanParenthesis } from "./electre.js"

describe('cleanVolume()', () => {
  test('returns nothing if volume numbering does not apply', () => {
    expect(cleanVolume('')).toBe('')
  })

  test('returns a volume number with 2 known Electre notations', () => {
    expect(cleanVolume('1 / ')).toBe('1')
    expect(cleanVolume('2')).toBe('2')
    expect(cleanVolume('3 / a')).toBe('3')
  })
})

describe('cleanPages()', () => {
  test('returns null if value is empty', () => {
    expect(cleanPages('')).toBeNull()
  })

  test('return a page number', () => {
    expect(cleanPages('1 vol. (183 p.) ;')).toBe(183)
    expect(cleanPages('1 vol. (653 p.)- 8 pl. :')).toBe(653)
  })
})

describe('cleanParenthesis()', () => {
  test('returns null if value is empty', () => {
    expect(cleanParenthesis('')).toBe('')
  })

  test('remove parenthesis', () => {
    expect(cleanParenthesis('(La découverte)')).toBe('La découverte')
    expect(cleanParenthesis('(Terre de poche,)')).toBe('Terre de poche')
    expect(cleanParenthesis('(Classiques & Cie. Collège ;)')).toBe('Classiques & Cie. Collège')
  })

  test('passthrough', () => {
    expect(cleanParenthesis('Soins aux enfants')).toBe('Soins aux enfants')
    expect(cleanParenthesis('Fantastique/Fantasy (romans ou nouvelles)')).toBe('Fantastique/Fantasy (romans ou nouvelles)')
  })
})
