export const CID_URL = '/~~import/~~write/web/u/cid'
export const WSP_URL = '/~~chain/web/u/wspSrc'
export const ATELIERS_URL = '/~~chain/web/u/search?cdaction=Search&param=wsp'
export const PRODUCTIONS_URL = '/~~import/?list'
export const EXPORTS_URL = '/~~write/tree?list'
export const STATIC_URL = '/~~static/fr-FR/home.xhtml#'

export const OUTPUT_FILE_FORMATS = Object.freeze({
  daisy2: {
    label: 'Daisy 2.02',
    extension: '.zip',
  },
  odtDuxbury: {
    label: 'Pivot Braille (ODT)',
    extension: '.odt',
  },
  xmlDTBook: {
    label: 'Pivot (xml dtbook)',
    extension: '.xml',
  },
})
