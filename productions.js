import { CID_URL, WSP_URL, ATELIERS_URL, PRODUCTIONS_URL, EXPORTS_URL } from "./constants.js"

/**
 * @typedef {Object} ProductionRecord
 * @property {ProductionRecordMetas} metas
 * @property {ProductionProcessingStatus} prc
 */

/**
 * @enum {string}
 */
 const ProductionProcessingStatus = {
  DATA: 'data',
  XML: 'xml'
}

/**
 * @typedef {Object} ProductionRecordMetas
 * @property {Array<ProductionOutput>} outputs
 * @property {Object<ProductionWorkshop>} workshop
 * @property {string} _status_id
 * @property {ProductionInternalStatus} _status
 * @property {string=} previousStatus
 * @property {string} author
 * @property {string} authorAud
 * @property {string} authorBrl
 * @property {string} collection
 * @property {string} comment
 * @property {number=} dateEnd
 * @property {number} deployDate
 * @property {string} deployedBy
 * @property {string} dureeAud
 * @property {string} illustrations
 * @property {string} isbn
 * @property {string} olderResId
 * @property {string} originalFilename
 * @property {string} originalLanguage
 * @property {string} pages
 * @property {string} path
 * @property {string} processing
 * @property {string} publisher
 * @property {string} resumeElectre
 * @property {Array<string>} source
 * @property {string} statut
 * @property {string} statutAud
 * @property {string} statutBrl
 * @property {string} statutEmb
 * @property {string} title
 * @property {string} typeOuvrage
 * @property {string} uid
 * @property {number} _year
 * @property {string} sourceXMLDoctype
 */

/**
 * @typedef {Object} ProductionWorkshop
 * @property {string} srcUri
 * @property {string} srcId
 * @property {string} lcSt
 * @property {string} itTi
 * @property {string} hasResume
 * @property {string} backResume
 * @property {string} publishingYear
 * @property {string} originalLanguage
 * @property {string} subTitle
 * @property {string} translatedTitle
 * @property {string} typeOuvrage
 * @property {string} infosAVH
 */

/**
 * @typedef {Object} ProductionOutput
 * @property {ProductionOutputMetas} metas
 * @property {string} n
 */

/**
 * @typedef {Object} ProductionOutputMetas
 * @property {string} path
 */

/**
 * @typedef {Object} ProductionInternalStatus
 * @property {string} label
 * @property {string} className
 * @property {number} weight
 */

export const RECORD_SCHEMA = Object.seal({
  // part of the CSV
  isbn: '',
  title: '',
  author: '',
  publisher: '',
  typeOuvrage: '',
  source: '',
  summary: '',
  collection: '',
  comment: '',
  volume: null,
  pages: null,
  illustrations: '',
  originalFilename: '',
  originalLanguage: 'fr',
  forChildren: false,
  statutBrl: '',
  statutAud: '',
  statutEmb: '',
  authorAud: '',
  authorBrl: '',
  // internal to Studio PAON
  statut: '',
  dateEnd: '',
  // internal to Scenari
  path: '',
  olderPath: '',
  olderResId: ''
})

const DEFAULT_HEADER = {
  'mode': 'cors',
}

/**
 * Persiste seulement les clés connues de `row`.
 *
 * @param {Object} row
 * @param {String} prc
 * @returns
 */
export function persistProduction (row, prc = null) {
  const SAVE_API_URL = `${CID_URL}?synch=true&scContent=none&createMetas=true${prc ? `&processing=${prc}` : ''}&returnProps=scCidSessStatus*scCidSessDetails*resId*path*processing*scCidSessId`

  const body = new FormData()
  Object.entries(row)
    .filter(([key]) => key in RECORD_SCHEMA)
    .forEach(([key, value]) => body.append(key, value))

  const SAVE_HEADER = {
    ...DEFAULT_HEADER,
    method: prc === 'data' ? 'PUT' : 'POST',
    body,
  }

  return fetch(SAVE_API_URL, SAVE_HEADER).then(response => response.text())
}

/**
 *
 * @param {ProductionRecordMetas} metas
 * @returns
 */
export function deleteProductionDocument(metas) {
  // TODO remplacer par la valeur obtenue par les métas
  const API_URL = `${WSP_URL}?cdaction=Delete&param=wsp`

  const body = new FormData()
  body.append('refUri', metas.workshop.srcUri)

  const REQUEST_HEADERS = {
    ...DEFAULT_HEADER,
    method: 'POST',
    body,
  }

  return fetch(API_URL, REQUEST_HEADERS).then(response => response.text())
}

export function setProductionStatus (statut, { metas }) {
  const { path: olderPath } = metas
  const extras = {}

  // we keep a reference to the previous computed status when switching in standby
  if (statut === STATUS_STANDBY) {
    extras.statut = `${STATUS_STANDBY}|${metas._status_id}`
  }
  else if (statut === STATUS_DONE) {
    extras.dateEnd = Date.now()
  } else {
    extras.dateEnd = ''
  }

  return persistProduction({ statut, olderPath, ...extras })
}

export function fetchProductionsAndOutputs () {
  const workshopListQuery = `<request>
  <select>
    <column dataKey="srcUri"/>
    <column dataKey="srcId"/>
    <column dataKey="lcSt"/>
    <column dataKey="itTi"/>
    <column dataKey="itAttr_hasResume"/>
    <column dataKey="itAttr_backResume"/>
    <column dataKey="itAttr_publishingYear"/>
    <column dataKey="itAttr_originalLanguage"/>
    <column dataKey="itAttr_subTitle"/>
    <column dataKey="itAttr_translatedTitle"/>
    <column dataKey="itAttr_typeOuvrage"/>
    <column dataKey="itAttr_infosAVH"/>
  </select>
  <where><and><exp type="Items"/></and></where>
</request>`

  const p = Promise.all([
    fetch(PRODUCTIONS_URL, DEFAULT_HEADER).then(response => response.json()),
    fetch(ATELIERS_URL, { ...DEFAULT_HEADER, method: 'POST', body: workshopListQuery}).then(response => response.json()),
    fetch(EXPORTS_URL, DEFAULT_HEADER).then(response => response.json())
  ])

  return p.then(([ imports, workshops, outputs ]) => {
    return imports.ch
      .filter(item => item.prc)
      .map(item => {
          const { uid, path } = item.metas

          const outputList = outputs.desc
            .find(({ n }) => n === uid)

          item.metas.outputs = outputList?.desc.filter(({ cdView }) => ['daisy2', 'odtDuxbury', 'xmlDTBook'].includes(cdView)) || []

          const workshop = workshops.results.find(workshop => workshop[0] === `${path}.book`)
          item.metas.workshop = workshop ? turnWorkshopArrayIntoObject(workshop, workshops.columns) : null

          return item
      })
      .map(addFileStatus)
      .sort(sortByStatus)
  })
}

/**
 *
 * @param {Array<string>} workshopArray
 * @param {Array<string>} columns
 * @returns {ProductionWorkshop}
 */
export function turnWorkshopArrayIntoObject (workshopArray, columns) {
  const ATTR_RE = /^itAttr_/

  return workshopArray.reduce((obj, value, index)=> ({
    ...obj,
    [ columns[index].replace(ATTR_RE, '') ]: value
  }), {})
}

export function uploadProductionPivotFile (fileToImport, metas) {
  var formData = new FormData();
  formData.append('cidContent', fileToImport, fileToImport.name);

  const IMPORT_HEADER = {
    ...DEFAULT_HEADER,
    method: 'POST',
    body: formData
  }

  return fetch(`${CID_URL}?synch=true&processing=xml&createMetas=true&olderPath=${metas.path}&returnProps=scCidSessStatus*scCidSessDetails*resId*path*processing*scCidSessId`, IMPORT_HEADER)
    .then(response => {
      if (!response.ok) {
        throw new Error('Une erreur est survenue lors de l\'import du fichier dans scenari.')
      }
    });
}

export const STATUS_UNKNOWN = 'UNKNOWN'
export const STATUS_PLATON_PENDING = 'PLATON_PENDING'
export const STATUS_PLATON_AWAITING = 'PLATON_AWAITING'
export const STATUS_STANDBY = 'STANDBY'
export const STATUS_PROCESSING = 'PROCESSING'
export const STATUS_TO_CONVERT = 'TO_CONVERT'
export const STATUS_EXPORTED = 'EXPORTED'
export const STATUS_DONE = 'DONE'

export const STATUSES = Object.freeze({
  [STATUS_UNKNOWN]: {
    label: 'inconnu',
    className: 'unknown',
    weight: -1,
    backgroundColor: '#ffffff'
  },
  [STATUS_PLATON_PENDING]: {
    label: 'Platon (à demander)',
    className: 'platonPending',
    weight: 1,
    backgroundColor: '#00b0f0'
  },
  [STATUS_PLATON_AWAITING]: {
    label: 'Platon (en attente)',
    className: 'platonAwaiting',
    weight: 5,
    backgroundColor: '#4472c4'
  },
  [STATUS_TO_CONVERT]: {
    label: 'à convertir',
    className: 'toConvert',
    weight: 10,
    backgroundColor: '#ffc000'
  },
  [STATUS_PROCESSING]: {
    label: 'en cours',
    className: 'processing',
    weight: 20,
    backgroundColor: '#ed7d31'
  },
  [STATUS_EXPORTED]: {
    label: 'exporté',
    className: 'exported',
    weight: 30,
    backgroundColor: '#a9d18e'
  },
  [STATUS_DONE]: {
    label: 'OK médiathèque',
    className: 'done',
    weight: 50,
    backgroundColor: '#92d050'
  },
  [STATUS_STANDBY]: {
    label: 'stand-by',
    className: 'standBy',
    weight: 90,
    backgroundColor: '#ffffff'
  },
})

export const FILE_FORMAT_PDF = 'PDF'
export const FILE_FORMAT_EPUB = 'EPUB'
export const FILE_FORMAT_XML = 'XML'
export const FILE_FORMAT_INDD = 'INDD'
export const FILE_FORMAT_DOCX = 'DOCX'

export const FILE_FORMATS = Object.freeze({
  [FILE_FORMAT_PDF]: {
    label: 'PDF'
  },
  [FILE_FORMAT_EPUB]: {
    label: 'ePub'
  },
  [FILE_FORMAT_XML]: {
    label: 'XML'
  },
  [FILE_FORMAT_INDD]: {
    label: 'InDesign'
  },
  [FILE_FORMAT_DOCX]: {
    label: 'Word (docx)'
  }
})

/**
 * Parses a Scenari stored metadata into something useable in the UI
 *
 * @param {string|undefined} sourceMeta
 * @returns {{ knownFormats: Array<string>, other: string}}
 */
export function splitFileFormats (sourceMeta) {
  return (sourceMeta || '').split('|').reduce((obj, value) => {
    if (value in FILE_FORMATS) {
      obj._source.knownFormats.push(value)
      obj.source.push(value)
    }
    else if (value) {
      obj._source.other = value
      obj.source.push(value)
    }
    return obj
  }, { source: [], _source: { knownFormats: [], other: null } })
}

export function lastUpdatedOn (metas) {
  const allTheDates = [
    metas.deployDate,
    parseFloat(metas.dateEnd),
    ...(metas.outputs ?? []).map(({ metas }) => metas.deployDate)
  ]
  .filter(date => Number.isNaN(date) === false)
  .filter(date => date !== undefined)

  return Math.max(...allTheDates)
}

export function sortByStatus (a, b) {
  if (a.metas._status.weight === b.metas._status.weight) {
    const mostRecentA = lastUpdatedOn(a.metas)
    const mostRecentB = lastUpdatedOn(b.metas)

    return mostRecentB - mostRecentA
  }
  else {
    return a.metas._status.weight - b.metas._status.weight
  }
}

/**
 * Augments a record with Status internal metadata
 *
 * @param {ProductionRecord} record
 * @return {ProductionRecord}
 */
export function addFileStatus (record) {
  record.metas = {
    ...record.metas,
    ...splitFileFormats(record.metas.source),
    ...getStatusAndClass(record),
    backgroundColor: record.backgroundColor,
    _year: new Date(record.metas.dateEnd ? parseFloat(record.metas.dateEnd) : record.metas.deployDate).getUTCFullYear()
  }

  return record
}

/**
 * @param {Object} statusesState
 * @returns {function(ProductionRecord): boolean}
 */
export function filterByStatus (statusesState) {
  return (record) => statusesState[record.metas._status_id] === true || record.metas._status_id === STATUS_UNKNOWN
}

/**
 * @param {Object} state
 * @returns {function(ProductionRecord): boolean}
 */
export function filterByYear (state) {
  return (record) => {
    return state[record.metas._year] === true
  }
}

/**
 * Derive a status from a record
 *
 * @param {ProductionRecord} record
 * @returns {{_status_id: string, _status: ProductionInternalStatus}}
 */
export function getStatusAndClass (record) {
  let _status_id = STATUS_UNKNOWN
  const {prc} = record
  const {outputs, workshop = null, statut = ''} = record.metas
  const [currentStatus, previousStatus] = statut.split('|')

  if (prc === 'data') {
    _status_id = STATUS_PLATON_PENDING

    if ([STATUS_STANDBY, STATUS_PLATON_AWAITING, STATUS_TO_CONVERT].includes(currentStatus)) {
      _status_id = currentStatus
    }
  }
  else if (prc === 'xml') {
    // There is a bug/behaviour in Scenari where it is impossible to remove a document, and modify its meta
    // The document is put back when we do that. So we extra check if there is a workshop object.
    // When there is no workshop object, it means we moved back from 'processing' to 'to convert'
    if ([STATUS_TO_CONVERT, STATUS_PROCESSING].includes(currentStatus) && workshop === null) {
      _status_id = STATUS_TO_CONVERT
    }
    else if ([STATUS_TO_CONVERT, STATUS_PROCESSING, '', undefined].includes(currentStatus)) {
      _status_id = outputs.length === 0 ? STATUS_PROCESSING : STATUS_EXPORTED
    }
    else if ([STATUS_STANDBY, STATUS_DONE].includes(currentStatus)) {
      _status_id = currentStatus
    }
  }

  return { _status_id, _status: STATUSES[_status_id], previousStatus }
}

/**
 * Nom des colonnes éditables dans la modale des métadonnées
 */
 export const EDITABLE_COLUMNS = [
  "Titre",
  "Auteur",
  "Éditeur",
  "Audio",
  "Braille",
  "Embossage",
  "Type d\'ouvrage",
  "Collection",
  "Nbr. de pages",
  "Illustrations",
  "Langue originelle",
  "Fichier source",
  "DTD Détail"
]

export const metadataModalSchema = [
  {
    column: 'Embossage',
    type: String,
    name: 'statutEmb',
    value: row => row.metas.statutEmb
  },
  {
    column: 'Date fin',
    type: Date,
    format: 'yyyy/mm/dd',
    value: row => row.metas.dateEnd ? new Date(row.metas.dateEnd) : null
  },
  {
    column: 'ISBN',
    type: String,
    value: row => row.metas.isbn
  },
  {
    column: 'Nom du fichier importé',
    type: String,
    name: 'originalFilename',
    value: row => row.metas.originalFilename
  },
  {
    column: 'Fichier source',
    type: String,
    value: row => row.metas.source.join(', ')
  },
  {
    column: 'DTD',
    type: String,
    value: row => row.metas.sourceXMLDoctype
  },
  {
    column: 'DTD Détail',
    type: String,
    value: row => row.metas.dtdDetail
  },
  {
    column: 'Durée enregistrement',
    type: String,
    value: row => row.metas.dureeAudio
  },
  {
    column: 'Titre',
    type: String,
    name: 'title',
    value: row => row.metas.title ?? row.metas?.workshop.itTi
  },
  {
    column: 'Auteur',
    type: String,
    name: 'author',
    value: row => row.metas.author
  },
  {
    column: 'Éditeur',
    type: String,
    name: 'publisher',
    value: row => row.metas.publisher
  },
  {
    column: 'Date de publication',
    type: String,
    value: row => row.metas.workshop?.publishingYear
  },
  {
    column: 'Type d\'ouvrage',
    type: String,
    name: 'typeOuvrage',
    value: row => row.metas.typeOuvrage
  },
  {
    column: 'Collection',
    type: String,
    name: 'collection',
    value: row => row.metas.collection
  },
  {
    column: 'Nbr. de pages',
    type: String,
    name: 'pages',
    value: row => row.metas.pages
  },
  {
    column: 'Illustrations',
    type: String,
    name: 'illustrations',
    value: row => row.metas.illustrations
  },
  {
    column: 'Langue originelle',
    type: String,
    name: 'originalLanguage',
    value: row => row.metas.originalLanguage
  },
]

function parseDateEnd ({ metas: { dateEnd } }) {
  const date = new Intl.DateTimeFormat('fr-FR')
  if (dateEnd && dateEnd !== '') {
    return date.format(new Date(parseFloat(dateEnd)))
  }
  return ''
}

export const excelExportSchema = [
  {
    column: 'Statut',
    type: String,
    value: row => row.metas._status.label,
    backgroundColor: row => row.metas._status.backgroundColor
  },
  {
    column: 'ISBN',
    type: String,
    value: row => row.metas.isbn
  },
  {
    column: 'Titre / Auteur',
    type: String,
    name: 'title',
    value: row => `${row.metas.title ?? row.metas?.workshop.itTi} / ${row.metas.author}`
  },
  {
    column: 'Éditeur',
    type: String,
    name: 'publisher',
    value: row => row.metas.publisher
  },
  {
    column: 'Embossage',
    type: String,
    name: 'statutEmb',
    value: row => row.metas.statutEmb
  },
  {
    column: 'Durée Daisy',
    type: String,
    value: row => row.metas.dureeAudio
  },
  {
    column: 'Fichier source',
    type: String,
    value: row => row.metas.source.join(', ')
  },
  {
    column: 'DTD',
    type: String,
    value: row => row.metas.sourceXMLDoctype
  },
  {
    column: 'DTD Détail',
    type: String,
    value: row => row.metas.dtdDetail
  },
  {
    column: 'Date fin',
    type: String,
    value: row => parseDateEnd(row)
  }
]

export const makeExcelData = (data) => {
  const excelData = [excelExportSchema
    .map(schemaRow => (
      { value: schemaRow.column,
        backgroundColor: '#FFD966',
        alignVertical: 'center',
        align: 'center',
        height: 40
      }))]
  data.map(row => {
    excelData.push(excelExportSchema.map(({ value, type, format, backgroundColor }) => {
      return {
        value: value(row),
        type,
        format,
        backgroundColor: backgroundColor ? backgroundColor(row) : '#B4C7E7'
      }
    }))
  })
  return excelData
}

