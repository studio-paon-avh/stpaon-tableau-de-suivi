import dialogPolyfill from 'https://cdn.skypack.dev/pin/dialog-polyfill@v0.5.6-iT3zRwwSB7HpypfS3n4U/mode=imports,min/optimized/dialog-polyfill.js'

import { refreshDashboard, displayNotifications } from './template.js'
import store from './store.js'

// Register `<dialog>` polyfill. It will be useless in a couple years.
dialogPolyfill.registerDialog(document.querySelector('dialog'))

// Inject Scenari theme preferences
if (window.frameElement && window.frameElement.injectTheme) {
  window.frameElement.injectTheme()
}

// Refresh dashboard on filters changes
store.select('filters').on('update', () => refreshDashboard())

// We persist filters preferences on change
store.select('filters').on('update', ({ data }) => {
  localStorage.setItem('filters', JSON.stringify(data.currentData))
})

// Display incoming notifications
store.select('notifications').on('update', ({ data }) => displayNotifications(data.currentData))

// Initialize Dashboard
refreshDashboard()
