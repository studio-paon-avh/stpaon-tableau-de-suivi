import { html, render } from 'https://cdn.skypack.dev/pin/lit-html@v2.0.0-kfI6Y9B9Hg1qgdLLWHKh/mode=imports/optimized/lit-html.js'
import { ref, createRef } from 'https://cdn.skypack.dev/pin/lit-html@v2.0.0-kfI6Y9B9Hg1qgdLLWHKh/mode=imports/optimized/lit-html/directives/ref.js'

import writeExcelFile from 'https://cdn.skypack.dev/pin/write-excel-file@v1.3.11-UIwciijr24AOLFUZKs9V/mode=imports/optimized/write-excel-file.js'
import readExcelFile from 'https://cdn.skypack.dev/pin/read-excel-file@v5.2.8-t7ddDey5P7YvKaaEcSLo/mode=imports/optimized/read-excel-file.js'

import store, { Store } from './store.js'
import { schema } from './electre.js'
import { fetchProductionsAndOutputs, setProductionStatus, deleteProductionDocument, filterByStatus, filterByYear, uploadProductionPivotFile, metadataModalSchema, persistProduction } from './productions.js'
import { STATIC_URL, OUTPUT_FILE_FORMATS } from "./constants.js"
import { STATUS_EXPORTED, STATUS_PLATON_PENDING, STATUS_PLATON_AWAITING, STATUS_TO_CONVERT, STATUS_PROCESSING, STATUS_STANDBY, STATUS_DONE, EDITABLE_COLUMNS } from './productions.js'
import { FILE_FORMATS, STATUSES, makeExcelData } from './productions.js'

const date = new Intl.DateTimeFormat('fr-FR')
const BRL_RE = /.brl^/i
const AUDIO_RE = /.(mp3|flac)^/i

const stateToggle = (store, key, value) => {
  const index = store.get(key).indexOf(value)

  index === -1 ? store.push(key, value) : store.unset([key, index])
}

/**
 * @typedef {Object} ProductionStatusesCount
 * @property {number} [STATUS_EXPORTED]
 * @property {number} [STATUS_PLATON_PENDING]
 * @property {number} [STATUS_PLATON_AWAITING]
 * @property {number} [STATUS_TO_CONVERT]
 * @property {number} [STATUS_PROCESSING]
 * @property {number} [STATUS_STANDBY]
 * @property {number} [STATUS_DONE]
 */

/**
 * @typedef {Object} Notification
 * @property {NotificationType=} type
 * @property {string} message
 * @property {string=} details
 */

/**
 * @enum {string} NotificationType
 */
const NotificationType = Object.freeze({
  ERROR: 'error',
  SUCCESS: 'success'
})

/**
 * @typedef { import("./production.js").ProductionRecord } ProductionRecord
 * @typedef { import("./production.js").ProductionRecordMetas } ProductionRecordMetas
 * @typedef { import("./production.js").ProductionInternalStatus } ProductionInternalStatus
 */

/**
 * Display
 *
 * @param {{ filteredFileList: Array<ProductionRecord> }} displayContext
 *
 * @returns {Promise<string>}
 */
export function displayTable ({ filteredFileList }) {
  console.log('liste des imports', filteredFileList)
  const table = document.getElementById('file-list')

  const template = html`<table>
    <thead>
        <tr>
            <th>Statut</th>
            <th>Utilisateur / Utilisatrice</th>
            <th>ISBN</th>
            <th>Titre</th>
            <th>Imports & Exports</th>
            <th>Commentaires</th>
            <th>Autres informations</th>
        </tr>
    </thead>
    <tbody>
      ${filteredFileList.map(record => displayTableRow(record))}
    </tbody>
  </table>`

  return render(template, table)
}

/**
 * Affichage de la case utilisateur d'une ligne du tableau
 * @param {{ metas: Object }} displayContext
 * @returns
 */
function displayUserColumn ({ metas }) {
  const { path: olderPath } = metas

  const state = new Store({
    olderPath
  })

  const sortByDeployDate = (a, b) => b.metas.deployDate - a.metas.deployDate
  const mostRecentBrailleOutput = metas.outputs
    .sort(sortByDeployDate)
    .find(output => BRL_RE.test(output.n))
  const mostRecentAudioOutput = metas.outputs
    .sort(sortByDeployDate)
    .find(output => AUDIO_RE.test(output.n))

  const submitStatusForm = async (event) => {
    event.preventDefault()
    await persistProduction({ ...state.get() })
    refreshDashboard()
  }

  return html`<form class="users-status" @submit=${submitStatusForm}>
    <label>
      Pivot
      <input
        readonly
        value=${metas._status_id !== STATUS_TO_CONVERT ? (metas.updatedBy || metas.deployedBy) : ''}
      >
    </label>

    <label>
      Braille
      ${mostRecentBrailleOutput ? html`<time datetime="${new Date(mostRecentBrailleOutput.metas.deployDate).toISOString()}">le ${date.format(mostRecentBrailleOutput.metas.deployDate)}</time>` : ''}
      <input
        value=${metas.statutBrl}
        @change=${e => state.set('statutBrl', e.target.value)}
      >
    </label>

    <label>
      Audio
      ${mostRecentAudioOutput ? html`<time datetime="${new Date(mostRecentAudioOutput.metas.deployDate).toISOString()}">le ${date.format(mostRecentAudioOutput.metas.deployDate)}</time>` : ''}
      <input
        value=${metas.statutAud}
        @change=${e => state.set('statutAud', e.target.value)}
      >
    </label>

    <div class="actions">
      <button type="submit">Enregistrer</button>
    </div>
  </form>`
}

/**
 * @param {string} cdView
 */
function displayExportLink ({ cdView, baseHref, contentName }) {
  const { label = '', extension = '' } = OUTPUT_FILE_FORMATS[cdView] || {}

  if (!label) {
    return null
  }

  const [filename] = (contentName || 'noname.xml').split(/\.[^\.]+$/)

  return html`<li><a href="${baseHref}${filename}${extension}" class="oneline" target="_top">${label}</a></li>`
}

/**
 * @param {ProductionRecord} record
 */
function displayTableRow ({ prc, metas }) {
  const title = metas.title || metas?.workshop.itTi || 'Sans titre'
  const { _status: status, _status_id: STATUS_ID } = metas
  const statusDate = new Date(metas.dateEnd ? parseFloat(metas.dateEnd) : metas.deployDate)

  return html`<tr class="production">
    <td class='oneline production-status' style='background-color: ${status.backgroundColor}'>
      ${status.label}<br>
      <time datetime="${statusDate.toISOString()}">(le ${date.format(statusDate)})</time>
      ${[STATUS_PROCESSING, STATUS_TO_CONVERT, STATUS_PLATON_AWAITING, STATUS_PLATON_PENDING].includes(STATUS_ID)
        ? html`<br>
          <button @click=${() => setProductionStatus(STATUS_STANDBY, { metas }).then(() => refreshDashboard())}>Mettre en stand-by</button>`
        : ''}
      ${(STATUS_ID === STATUS_PLATON_PENDING)
        ? html`<br>
          <button @click=${() => setProductionStatus(STATUS_PLATON_AWAITING, { metas }).then(() => refreshDashboard())}>Indiquer comme demandé à Platon</button>`
        : ''}
      ${(STATUS_ID === STATUS_PLATON_AWAITING)
        ? html`<br>
          <button @click=${() => setProductionStatus(STATUS_TO_CONVERT, { metas }).then(() => refreshDashboard())}>Indiquer comme réceptionné de Platon</button>`
        : ''}
      ${(STATUS_ID === STATUS_STANDBY)
          ? html`<br>
            <button @click=${() => setProductionStatus(metas.previousStatus, { metas }).then(() => refreshDashboard())}>Repasser en "${STATUSES[metas.previousStatus].label}"</button>
            <br><button @click=${() => persistProduction({ path: metas.path }, 'remove').then(() => refreshDashboard())}>Supprimer de la liste</button>`
          : ''}
      ${(STATUS_ID === STATUS_EXPORTED)
          ? html`<br>
            <button @click=${() => setProductionStatus(STATUS_DONE, { metas }).then(() => refreshDashboard())}>Envoyé en médiathèque</button>`
          : ''}
      ${(STATUS_ID === STATUS_DONE)
          ? html`<br>
            <button @click=${() => setProductionStatus('', { metas }).then(() => refreshDashboard())}>Repasser à exporté</button>`
          : ''}
    </td>
    <td>${displayUserColumn({ metas })}</td>
    <th scope="row">${metas.isbn}</th>
    <td>
      ${metas.workshop ? html`<a href="${STATIC_URL}(wspDoc'wsp'doc'/${metas.uid}.book'src'${metas.workshop.srcId}')" class="title" target="_top">${title}</a>` : html`<span class="title">${title}</span>`}
      ${metas.author && html`<br><span class="author">${metas.author}</span>`}
      ${metas.publisher && html`<br><span class="publisher">${metas.publisher}</span>`}
    </td>
    <td>
      ${STATUS_ID === STATUS_TO_CONVERT ? html`<button type="button" @click=${() => openDocumentUploadModal({ prc, metas })}>Importer un document</button>` : ''}
      ${STATUS_ID === STATUS_PROCESSING ? html`<button type="button" @click=${() => openDocumentRemovalModal({ prc, metas })}>Supprimer le document</button>` : ''}
      ${metas.outputs.length ? html`<ul>${metas.outputs.map(({metas : outputMetas, cdView, n}) => {
        return displayExportLink({ cdView, baseHref: `${outputMetas.path}/${n}&contentName=`, contentName: metas.originalFilename })
      })}
      </ul>` : ''}
    </td>
    <td>
      <pre class="comment">${metas.comment}</pre>
      <button @click=${() => openCommentModal({ metas })}>${metas.comment ? 'Modifier' : 'Commenter'}</button>
    </td>
    <td><ul>
    ${metas.typeOuvrage && html`<li><b>${metas.typeOuvrage}</b></li>`}
    
      ${metas.pages && html`<li>${metas.pages} pages</li>`}
      ${metas.source && html`<li><b>Source</b> ${metas.source.join(', ')}</li>`}
      <li class="oneline"><b>Résumé</b> ${metas.workshop?.hasResume
      ? html`<span aria-label="existant">✅</span>`
      : html`<span aria-label="absent">absent</span>`}
      </li>
      
      <li><button class="link" @click=${() => openProductionMetadataModal({ metas })}>Toutes les informations</button></li>
    </ul></td>
  </tr>`
}

/**
 *
 * @param {Array<ProductionRecord>} fileList
 * @return {Array<{ year: number }>}
 */
export function getProductionYears (fileList) {
  const currentYear = new Date().getUTCFullYear()
  return fileList.reduce((years, record) => {
    const year = record.metas._year

    years[ year ] = Number.isInteger(years[ year ]) ? years[ year ] + 1 : 1

    return years
  }, { [currentYear]: 0 })
}

/**
 *
 * @param {Array<ProductionRecord>} fileList
 * @return {ProductionStatusesCount}
 */
export function getCountPerStatus (fileList) {
  return fileList.reduce((count, record) => {
    const { _status_id } = record.metas
    count[ _status_id ] = Number.isInteger(count[ _status_id ]) ? count[ _status_id ]+1 : 1

    return count
  }, {})
}

/**
 * Crée les productions à partir d'un export Electre, si elles ne sont pas connues
 *
 * @param {{ fileInput: HTMLInputElement, fileList: Array<ProductionRecord> }} options
 * @returns {function(): void}
 */
function handleElectreFileUpload ({ fileInput, fileList }) {
  return async () => {
    const { rows } = await readExcelFile(fileInput.value.files[0], { schema })
    const filteredISBNs = []

    pushNotification({ message: `Démarrage de l'import de ${rows.length} livres…` })
    const rowPromises = rows
      // skip rows which are already known
      .filter(({ isbn }) => {
        if (fileList.find(({ metas }) => metas.isbn === isbn)) {
          filteredISBNs.push(isbn)
          return false
        }

        return true
      })
      .map(row => {
        return persistProduction({
          ...row,
          originalLanguage: row.originalLanguage || 'fr',
          statutEmb: 'Non'
        }, 'data')
      })

    try {
      await Promise.all(rowPromises)
      pushNotification({ type: 'success', message: `${rows.length - filteredISBNs.length} livres importés.` })

      if (filteredISBNs.length) {
        pushNotification({ type: 'warning', message: `Les références ${filteredISBNs.join(', ')} n'ont pas été importées, car déjà présentes dans la liste des productions.` })
      }
    }
    catch (error) {
      pushNotification({ type: 'error', message: `Une erreur s'est produite lors de l'import.`, details: error.message })
      console.error(error)
    }
    finally {
      refreshDashboard()
    }
  }
}

/**
 *
 * @param {{ fileList: Array<ProductionRecord>, filteredFileList: Array<ProductionRecord> }} context
 * @returns {Promise<string>}
 */
export function displayFileFilters ({ fileList, filteredFileList }) {
  const fileFiltersElement = document.getElementById('file-filters')

  const togglableStatuses = Object.entries(STATUSES).filter(([, status]) => status.weight >= 0)
  const state = store.select('filters')
  const values = state.get('statuses')
  const yearValues = state.get('years')

  const count = getCountPerStatus(fileList)
  const productionYears = getProductionYears(fileList)

  const exportFileList = toExcel.bind(null, fileList)
  const toggleState = (STATUS) => state.set(['statuses', STATUS], !values[STATUS])
  const toggleYearState = (year) => state.set(['years', year], !yearValues[year])
  const fileInput = createRef()

  const triggerElectreUpload = () => fileInput.value.click()

  const template = html`<div class="group">
    <strong>Filtrer par statut</strong> :
    ${togglableStatuses.map(([statusId, status]) => {
      return html`<label>
        <input type="checkbox" @click=${() => toggleState(statusId)} ?checked=${values[statusId]}>
        ${status.label}
        <small>(${count[statusId] ?? 0})</small>
      </label>`
    })}
  </div>

  <div class="group">
    <strong>Filtrer par année</strong> :
    ${Object.entries(productionYears).map(([year, count]) => html`<label>
      <input type="checkbox" @click=${() => toggleYearState(year)} ?checked=${yearValues[year]}>
      ${year}
      <small>(${count})</small>
    </label>`)}
  </div>

  <div class="group">
    <b>${filteredFileList.length} résultats</b> affichés.

    <span class="float-right">
      <button @click=${exportFileList} type="button">Export Excel de cette liste</button>

      <input type="file" name="csv" accept=".xlsx" @change=${handleElectreFileUpload({ fileInput, fileList })} ${ref(fileInput)} hidden>
      <button @click=${triggerElectreUpload} type="button">Import Electre</button>
    </span>
  </div>`

  return render(template, fileFiltersElement)
}

/**
 *
 * @param {*} param0
 * @returns
 */
export async function refreshDashboard () {
  const filters = store.select('filters').get()

  const fileList = await fetchProductionsAndOutputs()
  const filteredFileList = fileList
    .filter(filterByStatus(filters.statuses))
    .filter(filterByYear(filters.years))

  return Promise.all([
    displayTable({ filteredFileList }),
    displayFileFilters({ fileList, filteredFileList })
  ])
}

/**
 *
 * @param {Array<ProductionRecord>} fileList
 * @param {*} param1
 */
async function toExcel (fileList, { filename }) {
  let excelFilename = filename
  if (!filename) {
    const today = new Date()
    const year = today.getFullYear()
    const month = today.getMonth() + 1
    const day = today.getDate()
    excelFilename = `studio-paon_${year}${month > 9 ? month : '0' + month}${day > 9 ? day : '0' + day}.xlsx`
  }
  const blob = await writeExcelFile(makeExcelData(fileList))

  const url = URL.createObjectURL(blob, { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' })
  const link = document.createElement('a')
  link.href = url
  link.download = excelFilename
  link.rel = 'noopener'

  const evt = document.createEvent('MouseEvents')
  evt.initMouseEvent('click', true, true, window, 0, 0, 0, 80, 20, false, false, false, false, 0, null)
  link.dispatchEvent(evt)
}

/**
 *
 * @param {*} param0
 * @param {*} param1
 * @returns
 */
function openCommentModal ({ metas }) {
  const dialog = document.querySelector('dialog')
  const { path: olderPath, comment } = metas
  const state = new Store({
    // this property switches the call into a 'PATCH'-like request
    olderPath,
    comment,
  })

  const submitForm = async (event) => {
    event.preventDefault()
    await persistProduction({ ...state.get() })
    pushNotification({ type: NotificationType.SUCCESS, message: 'Commentaire sauvegardé.' })
    refreshDashboard()
    dialog.close()
  }

  render(html``, dialog)
  dialog.showModal()

  const template = html`
  <header>
    <h2><code>${metas.isbn}</code> ${metas.title}</h2>
  </header>
  <section>
    <form @submit=${submitForm}>
      <label>
        Commentaire
        <textarea name="comment" rows="10" @input=${(e) => state.set('comment', e.target.value)}>${comment}</textarea>
      </label>

      <div class="actions">
        <button type="submit">Enregistrer</button>
        <button type="button" class="cancel link" @click=${() => dialog.close()}>Annuler</button>
      </div>
    </form>
  </section>`

  return render(template, dialog)
}

function displayMetadataTable ({ metas, column, name, value, state }) {
  const valueToDisplay = value({ metas })
  let ddContent = valueToDisplay

  if (!name && EDITABLE_COLUMNS.includes(column)) {
    console.warn(`The name parameter is missing for the editable column ${column}`)
  }

  if (EDITABLE_COLUMNS.includes(column) && name) {
    if (column === "Embossage") {
      ddContent = html`<select @change=${e => state.set(name, e.target.value)} required>
        <option value="">Choisir une valeur --</option>
        <option value="Non" ?selected=${valueToDisplay.toLocaleLowerCase() === 'non'}>Non</option>
        <option value="Intégral" ?selected=${valueToDisplay.toLocaleLowerCase() === 'intégral'}>Intégral</option>
        <option value="Abrégé" ?selected=${valueToDisplay.toLocaleLowerCase() === 'abrégé'}>Abrégé</option>
        <option value="Intégral & abrégé" ?selected=${valueToDisplay.toLocaleLowerCase() === 'intégral & abrégé'}>Intégral &amp; abrégé</option>
      </select>`
    }
    else if (column === "Fichier source") {
      const otherOriginRef = createRef()
      const {knownFormats, other:otherOriginValue} = metas._source

      const onOriginSrcChanged = (event) => {
        if (event.target.checked) {
          otherOriginRef.value.hidden = false
          otherOriginRef.value.required = true
          state.set('otherOrigin', event.target.value)
        } else {
          otherOriginRef.value.hidden = true
          otherOriginRef.value.required = false
          state.set('otherOrigin', '')
        }
      }

      ddContent = html`
        ${Object.entries(FILE_FORMATS).map(([format, {label}]) => {
          return html `<label><input type="checkbox" name="source[]" value="${format}" @change=${e => stateToggle(state, 'source', e.target.value)} ?checked=${knownFormats.includes(format)}>${label}</label>`
        })}
        <label><input type="checkbox" @change=${e => onOriginSrcChanged(e)} ?checked=${otherOriginValue}>Autre</label>
        <input name="otherOrigin" type="text" @change=${e => state.set('otherOrigin', e.target.value)} value=${otherOriginValue} ${ref(otherOriginRef)} ?hidden=${!otherOriginValue}>
      `
    } else {
      ddContent = html`<input type="text" @change=${e => state.set(name, e.target.value)} value=${valueToDisplay}>`
    }
  }

  if (column === "Date fin" && valueToDisplay) {
    ddContent = html`<time datetime="${valueToDisplay.toISOString()}">${date.format(valueToDisplay)}</time>`
  }

  return html`
    <dt>${column}</dt>
    <dd>${ddContent}</dd>
  `
}

/**
 *
 * @param {Object} Production metadatas
 * @returns
 */
function openProductionMetadataModal ({ metas }) {
  const dialog = document.querySelector('dialog')

  const { path: olderPath, title } = metas
  const state = new Store({
    olderPath,
    filename: null,
    source: metas._source.knownFormats,
    otherOrigin: metas._source.other
  })

  const submitForm = async (event) => {
    event.preventDefault()

    // Finally, we replace the "otherOrigin" value by the user custom input
    state.set('source', [...state.get('source'), state.get('otherOrigin')].filter(d => d).join('|'))

    await persistProduction({ ...state.get() })
    pushNotification({ type: NotificationType.SUCCESS, message: html`Métadonnées de <em>${title}</em> sauvegardées.` })
    refreshDashboard()
    dialog.close()
  }

  render(html``, dialog)
  dialog.showModal()

  const template = html`<form @submit=${submitForm}>
  <div class="actions">
    <button type="submit">Valider les modifications </button>
    <button type="button" class="cancel link" @click=${() => dialog.close()}> Fermer</button>
  </div>

  <header>
    <h2><code>${metas.isbn}</code> ${metas.title}</h2>
  </header>

  <section>
    <dl>
      ${metadataModalSchema.map(({ column, name, value }) => {
        return displayMetadataTable({ metas, column, name, value, state })
      })}
    </dl>

  </section></form>`

  return render(template, dialog)
}

function openDocumentUploadModal({ prc, metas }) {
  const dialog = document.querySelector('dialog')
  const fileRef = createRef()
  const otherOriginRef = createRef()
  const { path: olderPath, title } = metas
  const { knownFormats, other:otherOriginValue } = metas._source
  const state = new Store({
    olderPath,
    filename: null,
    source: knownFormats,
    otherOrigin: otherOriginValue,
    statut: STATUS_PROCESSING
  })

  const submitForm = async (event) => {
    event.preventDefault()

    const [file] = fileRef.value.files
    state.set('originalFilename', file.name)

    // Finally, we merge source formats together
    state.set('source', [...state.get('source'), state.get('otherOrigin')].filter(d => d).join('|'))

    try {
      pushNotification({ message: html`Téléversement de <em>${title}</em> en cours…` })
      await uploadProductionPivotFile(file, metas)
      await persistProduction({ ...state.get() })
      pushNotification({ type: NotificationType.SUCCESS, message: `Téléversement réussi.` })
    }
    catch (error) {
      pushNotification({ type: NotificationType.ERROR, message: error.message ?? 'Une erreur s\'est produite lors de l\'import du document.' })
      console.error(error)
    }
    finally {
      refreshDashboard()
      dialog.close()
    }
  }

  const onOriginSrcChanged = (event) => {
    if (event.target.checked) {
      otherOriginRef.value.hidden = false
      otherOriginRef.value.required = true
      state.set('otherOrigin', event)
    } else {
      otherOriginRef.value.hidden = true
      otherOriginRef.value.required = false
      state.set('otherOrigin', '')
    }
  }

  render(html``, dialog)
  dialog.showModal()

  const template = html`
  <header>
    <h2><code>${metas.isbn}</code> ${metas.title}</h2>
  </header>
  <section>
    <form @submit=${submitForm}>
      <p>
        <label>
          Fichier à importer
          <input type="file" accept=".xml" required ${ref(fileRef)}>
        </label>
      </p>

      <p>
        Source d'origine
        ${Object.entries(FILE_FORMATS).map(([format, {label}]) => {
          return html `<label><input type="checkbox" name="source[]" value="${format}" @change=${e => stateToggle(state, 'source', e.target.value)} ?checked=${knownFormats.includes(format)}>${label}</label>`
        })}
        <label><input type="checkbox" @change=${e => onOriginSrcChanged(e)} ?checked=${otherOriginValue}>Autre</label>
        <input name="otherOrigin" type="text" value=${otherOriginValue} @change=${e => state.set('otherOrigin', e.target.value)} ${ref(otherOriginRef)} ?hidden=${!otherOriginValue}>
      </p>
      <div class="actions">
        <button type="submit">Enregistrer</button>
        <button type="button" class="cancel link" @click=${() => dialog.close()}>Annuler</button>
      </div>
    </form>
  </section>
  `

  return render(template, dialog)
}

function openDocumentRemovalModal({ metas }) {
  const dialog = document.querySelector('dialog')
  const { title } = metas

  const submitForm = async (event) => {
    event.preventDefault()

    try {
      pushNotification({ message: html`Suppression du document de <em>${title}</em> en cours…` })
      // uncomment this when Scenari bug/feature is fixed
      // otherwise it "undo" the document deletion
      // await setProductionStatus(STATUS_TO_CONVERT, { metas })
      await deleteProductionDocument(metas)
      pushNotification({ type: NotificationType.SUCCESS, message: `Suppression du document réussie.` })
    }
    catch (error) {
      pushNotification({ type: NotificationType.ERROR, message: error.message ?? 'Une erreur s\'est produite lors de la suppression du document.' })
      console.error(error)
    }
    finally {
      refreshDashboard()
      dialog.close()
    }
  }

  render(html``, dialog)
  dialog.showModal()

  const template = html`
  <header>
    <h2><code>${metas.isbn}</code> ${metas.title}</h2>
  </header>
  <section>
    <form @submit=${submitForm}>
      <p>
        Confirmez-vous la suppression du document ?<br>
        Cela entraine la perte de tout travail réalisé dessus.
      </p>

      <p>
        Vous aurez la possibilité de téléverser un nouveau document après sa suppression.
      </p>

      <div class="actions">
        <button type="submit">Je confirme la suppression</button>
        <button type="button" class="cancel link" @click=${() => dialog.close()}>Annuler</button>
      </div>
    </form>
  </section>
  `

  return render(template, dialog)
}



/**
 * Add a new notification in the stack
 *
 * @param {Notification} notification
 * @returns {Promise<string>}
 */
export function displayNotifications (notifications) {
  const list = document.querySelector('#notifications-list')

  const template = notifications.map(({ type, message }) => {
    return html`<li class="notification notification--${type}">${message}</li>`
  })

  return render(template, list)
}

/**
 * @param {Notification} notification
 */
export function pushNotification (notification) {
  store.push('notifications', notification)
}
