import Store from 'https://cdn.skypack.dev/pin/baobab@v2.6.1-fFljFW0hmYeYjGtohEyZ/mode=imports/optimized/baobab.js'

import { STATUS_EXPORTED, STATUS_TO_CONVERT, STATUS_PROCESSING, STATUS_DONE } from './productions.js'

const localStorageFilters = JSON.parse(localStorage.getItem('filters')
  || '{ "statuses" : {}, "years": {}}')

const tree = new Store({
  notifications: [],
  filters: {
    statuses: {
      [STATUS_EXPORTED]: true,
      [STATUS_TO_CONVERT]: false,
      [STATUS_PROCESSING]: true,
      [STATUS_DONE]: false,
      ...localStorageFilters.statuses
    },
    years: {
      [new Date().getUTCFullYear()]: true,
      ...localStorageFilters.years
    }
  }
})

export default tree
export { Store }
