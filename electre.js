export function cleanPublisher (value) {
  return String(value).replace(/[,\s]+$/g, '')
}

/**
 * Clean "Mot matière"
 * @param {string} value
 * @returns {string}
 */
export function cleanParenthesis (value) {
  return String(value).trim().replace(/^\((.+)\)$/g, '$1').replace(/[;,\s]+$/, '')
}

export function cleanVolume (value) {
  return value ? String(value).replace(/^(\d+)\D*/, "$1") : ''
}

export function cleanPages (value) {
  return value ? parseInt(String(value).match(/\d*\((\d+) p.\)\d*/)[1]) : null
}

export function cleanLanguage (value) {
  return value && value !== '' ? String(value) : 'fr'
}

/**
 * Chaque `prop` doit correspondre à une clé du `RECORD_SCHEMA` de `productions.js`
 */
export const schema = {
  "ISBN": {
    prop: 'isbn',
    type: String,
    required: true
  },
  "Titre": {
    prop: 'title',
    type: String,
    required: true
  },
  "Auteur": {
    prop: 'author',
    type: String
  },
  "Editeur": {
    prop: 'publisher',
    type: cleanPublisher
  },
  "Mot matière": {
    prop: 'typeOuvrage',
    type: cleanParenthesis
  },
  "Résumé": {
    prop: 'summary',
    type: String,
  },
  "Collection": {
    prop: 'collection',
    type: cleanParenthesis
  },
  "Tome": {
    prop: 'volume',
    type: cleanVolume
  },
  "Collation 1": {
    prop: 'pages',
    type: cleanPages
  },
  "Collation 2": {
    prop: 'illustrations',
    type: String
  },
  "Langue originale": {
    prop: 'originalLanguage',
    type: cleanLanguage
  },
  "Jeunesse": {
    prop: 'forChildren',
    type: Boolean
  }
}
