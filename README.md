# Module Scenari de Studio PAON

Studio PAON est un module complémentaire au logiciel Scenari pour gérer des chaînes de publication.

C'est une application dite "Single Page App" HTML/CSS/JS. Elle est conçue sans framework et sans outillage.

Quelques librairies aident à gérer quelques aspects (affichage, états intermédiaires, Excel). Ces librairies ont recours au service _open source_ externe [Skypack](https://www.skypack.dev/).

## Préparer son espace de travail

**Pré-requis** : Node.js v14 minimum.

La première étape est d'installer les dépendances du projet. Elles servent uniquement au développement en local.

```bash
npm install
```

La seconde est dernière étape est de relier votre environnement local à l'instance de Scenari à partir de laquelle vous souhaitez travailler :

1. Créez un fichier texte `.env` à partir de `.env.example` (`cp .env.example .env`)
2. Modifiez les valeurs du fichier `.env`

Pour déterminer la valeur de `SCENARI_BASIC_AUTH` :

```bash
echo -n '<utilisateur scenari>:<mot de passe>' | base64

## Par exemple, si "f+MuDygZ4D2" est le mot de passe associé à l'utilisateur "ext-thp"
# echo -n 'ext-thp:f+MuDygZ4D2' | base64
## Le résultat est
# ZXh0LXRocDpmK011RHlnWjREMg==
```

Cette valeur sert aussi à la [commande de déploiement manuel](#déployer-lapplication-frontend).

## Développer localement

```bash
npm start
```

Ouvrir [`localhost:5000`](http://localhost:5000) et admirer.

## Déployer l'application frontend

**Automatiquement**, via l'intégration continue :

- Chaque commit de chaque branche est déployé sur "int"
- Chaque commit sur la branche `master` est déployé en "prod"

**Manuellement**, en procédant ainsi :

- Zipper les fichiers contenus _dans_ le répertoire `scenari-module`
- Les importer dans Scenari (menu `Import › Rendu › Importer…`)

![Écran d'import Scenari](scenari-import.png)

En ligne de commande, ça revient à faire ceci :

```bash
$ cd scenari-module
$ zip -r upload.zip . -x '*.git*' -x '*node_modules*'
$ curl -v -X POST -F "cidContent=@upload.zip" -F 'processing=home' -F 'path=' -H "Authorization: Basic $SCENARI_BASIC_AUTH" 'https://studio-paon-int.scenari.ovh/~~import/~~write/web/u/cid?cdaction=StartSession&createMetas=true&scResp=200-403'
```

**Remarque** :

## URL des environnements
- dev : https://studio-paon-int.scenari.ovh/
- démo : https://studio-paon-qua.scenari.ovh/
- prod : https://studio-paon.scenari.eu/

## Correspondance dev/prod

- http://127.0.0.1:8164/Editadapt/editadapt~_all/wa/import/web -> /~~import/~~write/web
- http://127.0.0.1:8164/Editadapt/editadapt~_all/wa/import/tree -> /~~import
- http://127.0.0.1:8164/Editadapt/editadapt~_all/web -> /~~static
- http://127.0.0.1:8164/Editadapt/editadapt~_all/wa/depot/web ->  /~~write/web
- http://127.0.0.1:8164/Editadapt/editadapt~_all/wa/depot/tree/* ->  /~~write/tree/*

### Enregistrement d'une ligne de data

PUT http://127.0.0.1:8164/Editadapt/editadapt~_all/wa/import/web/u/cid?synch=true&scContent=none&createMetas=true&test=Toto&title=Mon Super Titre&processing=data&isbn=test

Authorization: Basic admin admin

### Import d'un fichier XML associé à une ligne de data
PUT http://127.0.0.1:8164/Editadapt/editadapt~_all/wa/import/web/u/cid?synch=true&createMetas=true&processing=xml&olderPath=/FR-AVH-ISBN-test
Authorization: Basic admin admin

< /home/tha/Downloads/9782081451452_DerniereNouvelleMars.xml

### Import d'un fichier XML "from scratch"
PUT http://127.0.0.1:8164/Editadapt/editadapt~_all/wa/import/web/u/cid?synch=true&createMetas=true&processing=xml&isbn=test2
Authorization: Basic admin admin

< /home/tha/Downloads/9782081451452_DerniereNouvelleMars.xml

FIchier XML si envoi via formulaire : cidContent

### Liste des datas d'import
GET http://127.0.0.1:8164/Editadapt/editadapt~_all/wa/import/tree/?list
Authorization: Basic admin admin

<> 2021-07-02T111520.200.json

```json{
  "n": "",
  "idx": 0,
  "t": "home",
  "layer": "prod",
  "prc": "home",
  "userRolesMap": {},
  "roles": [
    "main:admin"
  ],
  "resId": "1z0000A000",
  "m": 1625490874363,
  "path": "/",
  "permaPath": "",
  "publicUrl": "https://studio-paon-int.scenari.ovh/~~import/",
  "rootUrl": "https://studio-paon-int.scenari.ovh/~~import",
  "permaLink": "https://studio-paon-int.scenari.ovh/~~import/",
  "metas": {
    "path": "",
    "processing": "home",
    "deployedBy": "ext-thp",
    "olderResId": "1y0000A000",
    "deployDate": 1625490874361
  },
  "ch": [
    {
      "prc": "xml",
      "metas": {
        "updateDate": 1625231209578,
        "updatedBy": "tha",
        "author": "",
        "isbn": "TOTO",
        "overrideViews": {
          "views": {
            "dtbook": {
              "forceRemakeInMetas": true
            }
          }
        },
        "deployedBy": "tha",
        "title": "",
        "uid": "FR-AVH-ISBN-TOTO",
        "path": "/FR-AVH-ISBN-TOTO",
        "isLg": "true",
        "processing": "xml",
        "olderResId": "1u0000A000",
        "deployDate": 1625228327887
      }
    },
    {
      "prc": "xml",
      "metas": {
        "updateDate": 1625231208728,
        "updatedBy": "tha",
        "author": "",
        "isbn": "testTha",
        "overrideViews": {
          "views": {
            "dtbook": {
              "forceRemakeInMetas": true
            }
          }
        },
        "deployedBy": "tha",
        "title": "",
        "uid": "FR-AVH-ISBN-testTha",
        "path": "/FR-AVH-ISBN-testTha",
        "isLg": "true",
        "processing": "xml",
        "olderResId": "1t0000A000",
        "deployDate": 1625218125709
      }
    }
  ]
}
```

### Import d'un fichier export supplémentaire
PUT http://127.0.0.1:8164/Editadapt/editadapt~_all/wa/depot/web/u/cid?synch=true&createMetas=true&processing=file&path=/FR-AVH-ISBN-978-2-01-000923-5/mon-fichier-sorti
Authorization: Basic admin admin

< /home/tha/Downloads/978-2-01-000923-5_dtmodel

### liste des datas d'export
GET http://127.0.0.1:8164/Editadapt/editadapt~_all/wa/depot/tree/?list
Authorization: Basic admin admin

### Import du dashBoard
PUT http://127.0.0.1:8164/Editadapt/editadapt~_all/wa/import/web/u/cid?synch=true&createMetas=true&processing=home&path=/
Authorization: Basic admin admin

< /home/tha/Downloads/<nom>.zip

### Thème
if (window.frameElement && window.frameElement.injectTheme) window.frameElement.injectTheme();

## Tester
Le framework [Jest](https://jestjs.io/fr/) est utilisé pour les tests Javascript.
Les tests sont lancés en local avec la commande :
```
npm test
```

Les tests sont intégrés dans la CI de gitlab.
